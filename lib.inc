section .data
minus: db '-'
newline: dq 0xA

section .text
; Принимает код возврата и завершает текущий процесс

exit:  
    mov rax, 60
    syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.func:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .func
.end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, 10
    mov rax, rdi    
    mov rdi, rsp    
    dec rdi         
    push 0
    sub rsp, 16
.func:
    xor rdx, rdx
    div r9
    add rdx, '0'
    dec rdi
    mov byte[rdi], dl
    test rax, rax
    jnz .func
    call print_string
    add rsp, 24
    ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi	
    jnl print_uint
    push rdi
    mov rdi, [minus]
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:  
    mov al, byte [rdi]
    cmp al, byte [rsi]  
    jne .end 
    test al, al
    jz .strings_are_equal  
    inc rdi
    inc rsi
    jmp .loop
.strings_are_equal:
    mov rax, 1
    ret
.end:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov  r12, rdi
    mov  r13, rsi
    xor  r14, r14
.trim:
    call read_char
    cmp  al, ' '
    je   .trim
    cmp  al, `\t`
    je   .trim
    cmp  al, `\n`
    je   .trim
.loop:
    test al, al
    jz   .end
    cmp  r14, r13
    je  .drop
    cmp  al, ' '
    je   .end
    cmp  al, `\t`
    je   .end
    cmp  al, `\n`
    je   .end
    mov  [r12+r14], al
    inc  r14
    call read_char
    jmp  .loop
.drop:
    xor  rax, rax
    pop  r12
    pop  r13
    pop  r14
    ret
.end:
    cmp  r14, r13
    je   .drop
    mov  rax, r12
    mov  byte [r12+r14], 0
    mov  rdx, r14
    pop  r12
    pop  r13
    pop  r14
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push rcx
    xor rcx, rcx
    push rbx
    xor rbx, rbx
.check:
    mov bl, [rdi + rcx]
    cmp bl, '0'
    jb .end
    cmp bl, '9'
    ja .end
    sub rbx, '0'
    mul qword [newline]
    add rax, rbx
    inc rcx
    jmp .check
.end:
    mov rdx, rcx
    pop rbx
    pop rcx
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rcx, rcx
    mov rax, [rdi]
    cmp al, '+'
    jne .check_neg
    jmp .func
.check_neg:
    cmp al, '-'
    jne .uint
    inc rcx
    jmp .func
.uint:
    jmp parse_uint
.func:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx
    cmp rcx, 0
    je .end
    neg rax
.end:
    ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .ovf
    xor rcx, rcx
.func:
    xor r8, r8
    mov r8b, byte[rdi+rcx]
    mov byte[rsi+rcx], r8b
    cmp r8b, 0
    je .end
    inc rcx
    jmp .func
.end:
    ret
.ovf:
    xor rax, rax
    jmp .end
